function utilidadesJS() {}
utilidadesJS.orietacionImagenes = function(selector, classHorizontal, classVertical, classCuadrada) {
    if (classHorizontal == undefined) {
        classHorizontal = 'imagen_horizontal'
    }
    if (classVertical == undefined) {
        classVertical = 'imagen_vertical'
    }
    if (classCuadrada == undefined) {
        classCuadrada = 'imagen_cuadrada'
    }
    var imagenes = document.querySelectorAll(selector);
    if (imagenes[0].tagName == 'IMG') {
        for (var i = 0; i < imagenes.length; i++) {
            var anchoNatural = imagenes[i].naturalWidth;
            var altoNatural = imagenes[i].naturalHeight;
            if (anchoNatural > altoNatural) {
                imagenes[i].classList.add(classHorizontal);
            } else if (altoNatural > anchoNatural) {
                imagenes[i].classList.add(classVertical);
            } else {
                imagenes[i].classList.add(classCuadrada);
            }
        }
    } else {
        console.log('Ingresa un selector de imagen valido');
    }
};
$(window).on("load", function() {
    $(".loader").addClass("hide");
});
$(document).ready(function() {
    function openModal(e, element) {
        if (!element.hasClass("modal")) {
            e.preventDefault();
            var valueData = element.data("modal");
            $(".modal").addClass("hide");
            $(".modal[data-modal=" + valueData + "]").removeClass("hide");
        }
    }
    $("[data-modal]").on("click", function(e) {
        openModal(e, $(this));
    });
    $(".modal, .modal__close").on("click", function(e) {
        if (e.target == this) {
            $(".modal").addClass("hide");
        }
    });

    // CHANGE TITLE

    var mensage = `9 Gigas, más
    beneficios, más fácil.`;
    var mensage1 = `9 Gigas, más
    beneficios, más fácil.`;
    var foco = true;

    function changetitle() {
        $(".showTitleI").text('');
        if (foco) {
            $(".showTitleI").text(mensage1);
            foco = false;
        } else {
            $(".showTitleI").text(mensage);
            foco = true;
        }
    }
    // 

    function isMobile() {
        var wWindow = $(window).outerWidth();
        if (wWindow < 768) {
            return true;
        } else {
            return false;
        }
    }
    $(".nav__item__link, .bigBox--planes .bigBox__mini a, .logo").on("click", function() {
        $(".loader").removeClass("hide");
    });
    if ($(".owl-carousel").length > 0) {
        var sync1 = $(".carousel--intro .carousel__core.owl-carousel, .carousel--introB .carousel__core.owl-carousel");
        var sync2 = $(".carousel--intro .carousel__info.owl-carousel, .carousel--introB .carousel__info.owl-carousel");
        sync1.owlCarousel({
            items: 1,
            smartSpeed: 1000,
            autoplaySpeed: 1000,
            pagination: false,
            nav: true,
            navContainer: ".carousel__nav",
            afterAction: syncPosition,
            onTranslated: changetitle,
        }).on('changed.owl.carousel', function(e) {
            syncPosition(e.item.index);
        });
        sync2.owlCarousel({
            items: 1,
            smartSpeed: 1000,
            mouseDrag: false,
            pagination: false,
            responsiveRefreshRate: 100,
            afterAction: syncPosition2,
            animateOut: activateAnimOut(),
            animateIn: activateAnimIn(),
            afterInit: function(el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        }).on('changed.owl.carousel', function(e) {
            syncPosition2(e.item.index);
        });

        function activateAnimOut() {
            if (!isMobile()) {
                return "fadeOut";
            } else {
                return false;
            }
        }

        function activateAnimIn() {
            if (!isMobile()) {
                return "fadeIn";
            } else {
                return false;
            }
        }

        function syncPosition(el) {
            sync2.trigger('to.owl.carousel', el);
        }

        function syncPosition2(el) {
            sync1.trigger('to.owl.carousel', el);
        }
        $(".carousel--plan.owl-carousel").owlCarousel({
            loop: true,
            autoplay: true,
            smartSpeed: 1000,
            autoplaySpeed: 1000,
            pagination: false,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });        

        $(".carousel--planB.owl-carousel").owlCarousel({
            loop: true,
            autoHeight:true,
            // autoplay: true,
            smartSpeed: 1000,
            autoplaySpeed: 1000,
            pagination: false,
            // nav: true,
            // dots: false,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 5
                }
            }
        });
    }
    $(window).on("scroll", function() {
        var ctScroll = $(window).scrollTop();
        if (ctScroll > 100) {
            $(".carousel--intro .carousel__nav").addClass("down");
        } else {
            $(".carousel--intro .carousel__nav").removeClass("down");
        }
    });
    AOS.init();

    function forItems(comparative) {
        $(".boxPhones__item").addClass("hidden");
        $(".boxPhones__item").each(function() {
            var dataItem = $(this).data("phones"),
                dataAllItem = dataItem.split(" ");
            for (var i = 0; i <= dataAllItem.length; i++) {
                if (dataAllItem[i] == comparative) {
                    $(this).removeClass("hidden");
                }
            }
        });
    }
    if ($(".boxPhones__item").length > 0) {
        var dataActive = $(".boxPhones__nav__item.active").data("phones");
        forItems(dataActive);
    }
    $(".boxPhones__nav__item").on("click", function() {
        var dataNav = $(this).data("phones");
        $(".boxPhones__loader").removeClass("hide");
        setTimeout(function() {
            $(".boxPhones__loader").addClass("hide");
        }, 1000);
        $(".boxPhones__nav__item").removeClass("active");
        $(this).addClass("active");
        setTimeout(function() {
            forItems(dataNav);
        }, 300);
    });
});
$("#form_footer").submit(function(e) {
    e.preventDefault();
    var numeroTel = $("#form_telefono_footer").val();
    var url = "/portabilidad/gracias";
    if ($("#phone1:checked").length > 0) {
        var numDepa = $("#select-dep_footer").val();
        numeroTel = numDepa + numeroTel;
    }
    var laurl = window.location.href;
    var slug = laurl;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/llamar",
        "method": "POST",
        "headers": {
            "authorization": "AAAA-SO",
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "1c2ffa54-855f-7bd3-25b2-e70344a321a7"
        },
        "processData": false,
        "data": '{"api_key":"AAAA-SO","numero": "' + numeroTel + '","slug":"' + slug + '"}'
    };
    $.ajax(settings).done(function(response) {
        window.location.href = '/portabilidad/gracias';
        $(".gracias_principal").removeClass("hide");
    }).fail(function() {
        $(".gracias_principal").removeClass("hide");
    });
    $.ajax(settings).done(function(response) {
        $(".modal_footer").removeClass("hide");
        $("#form_footer").hide();
    }).fail(function() {
        $(".modal_footer").removeClass("hide");
        $("#form_footer").hide();
    });
});
$("#form_contacto").submit(function(e) {
    e.preventDefault();
    var numeroTel = $("#num_tel").val();
    var url = "/portabilidad/gracias";
    if ($("#phone3:checked").length > 0) {
        var numDepa = $("#select-dep").val();
        numeroTel = numDepa + numeroTel;
    }
    var laurl = window.location.href;
    var slug = laurl;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/llamar",
        "method": "POST",
        "headers": {
            "authorization": "AAAA-SO",
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "1c2ffa54-855f-7bd3-25b2-e70344a321a7"
        },
        "processData": false,
        "data": '{"api_key":"AAAA-SO","numero": "' + numeroTel + '","slug":"' + slug + '"}'
    };
    $.ajax(settings).done(function(response) {
        window.location.href = '/portabilidad/gracias';
        $(".gracias_principal").removeClass("hide");
    }).fail(function() {
        $(".gracias_principal").removeClass("hide");
    });
});
$(".carousel__item").click(function() {
    $(".modal").removeClass("hide");
});
$("#btn_click_modal").click(function(e) {
    e.preventDefault();
    $(".modal").removeClass("hide");
});
$("#contrata_btn_modal").click(function(e) {
    e.preventDefault();
    $(".modal").removeClass("hide");
});
$("#form_contacto_slider").submit(function(e) {
    e.preventDefault();
    var numeroTel = $("#num_tel_slider").val();
    var url = "/portabilidad/gracias";
    if ($("#phone3:checked").length > 0) {
        var numDepa = $("#select-dep").val();
        numeroTel = numDepa + numeroTel;
    }
    var laurl = window.location.href;
    var slug = laurl;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/llamar",
        "method": "POST",
        "headers": {
            "authorization": "AAAA-SO",
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "1c2ffa54-855f-7bd3-25b2-e70344a321a7"
        },
        "processData": false,
        "data": '{"api_key":"AAAA-SO","numero": "' + numeroTel + '","slug":"' + slug + '"}'
    };
    $.ajax(settings).done(function(response) {
        window.location.href = '/portabilidad/gracias';
        $(".gracias_principal").removeClass("hide");
    }).fail(function() {
        $(".gracias_principal").removeClass("hide");
    });
});
$(".carousel__item").click(function() {
    $(".modal").removeClass("hide");
});
$("#btn_click_modal").click(function(e) {
    e.preventDefault();
    $(".modal").removeClass("hide");
});
$("#contrata_btn_modal").click(function(e) {
    e.preventDefault();
    $(".modal").removeClass("hide");
});
$("#form_contacto_modal").submit(function(e) {
    e.preventDefault();
    var numeroTel = $("#num_tel_modal").val();
    if ($("#phone6:checked").length > 0) {
        var numDepa = $("#select-dep_modal").val();
        numeroTel = numDepa + numeroTel;
    }
    var laurl = window.location.href;
    var slug = laurl;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/llamar",
        "method": "POST",
        "headers": {
            "authorization": "AAAA-SO",
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "1c2ffa54-855f-7bd3-25b2-e70344a321a7"
        },
        "processData": false,
        "data": '{"api_key":"AAAA-SO","numero": "' + numeroTel + '","slug":"' + slug + '"}'
    };
    $.ajax(settings).done(function(response) {
        $(".modal_gracias").removeClass("hide");
    }).fail(function() {
        $(".modal_gracias").removeClass("hide");
    });
});

$(document).ready(function(){
  
    $(".titleTerms").on("click", function(){
        var aside = $(this).parents(".aside"),
        asidePos = aside.index() + 1;
        $(".aside").not(":nth-of-type(" + asidePos + ")").removeClass("active");
        $(".aside:nth-of-type(" + asidePos + ")").toggleClass("active");      
    });
});

let selectPlan = import('./selectPlan.js');