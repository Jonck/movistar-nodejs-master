$( document ).ready(function() {
    if( $('.planF') &&  $('.planI')){

        let planF =$('.planF'),
            planI = $('.planI');

        function selectPlanIndividual(){
            $(".planF").removeClass("selectPlan");
            $(".planI").removeClass("unselectPlan");
            $(".planF").addClass("unselectPlan");
            $(".planI").addClass("selectPlan");
            //plans
            $(".individual").removeClass("hidden");
            $(".family").addClass("hidden");
            
        }
        
        function selectPlanFamily(){
            $(".planI").removeClass("selectPlan");
            $(".planF").removeClass("unselectPlan");
            $(".planI").addClass("unselectPlan");
            $(".planF").addClass("selectPlan");
            //plans
            $(".family").removeClass("hidden");
            $(".individual ").addClass("hidden");
            
        }

        planF.click(selectPlanFamily);
        planI.click(selectPlanIndividual);
    }    
});

    