// Visualización de Planes
dataLayer.push({
    'ecommerce': {
        'promoView': {
            'promotions': [{
                    'name': '50 megas',
                    'position': '0',
                    'precioPromo': '98900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '100 megas | 2 Meses Gratis',
                    'position': '1',
                    'precioPromo': '132900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '200 megas | 2 Meses Gratis',
                    'position': '2',
                    'precioPromo': '157900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '300 megas | 2 Meses Gratis',
                    'position': '3',
                    'precioPromo': '202900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '50 megas',
                    'position': '00',
                    'precioPromo': '70900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '100 megas | 1 Meses Gratis',
                    'position': '01',
                    'precioPromo': '92900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '200 megas | 1 Meses Gratis',
                    'position': '02',
                    'precioPromo': '139900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '300 megas | 1 Meses Gratis',
                    'position': '03',
                    'precioPromo': '184900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                }
            ],
        },
    },
    'event': 'viewPromo'
});

//Clics de Planes
dataLayer.push({
    'ecommerce': {
        'promoClick': {
            'promotions': [{
                    'name': '60 GB de Internet',
                    'position': '0',
                    'precioPromo': '199990.00',
                    'categoriaPromo': 'Portabilidad',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '100 megas | 2 Meses Gratis',
                    'position': '1',
                    'precioPromo': '132900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '200 megas | 2 Meses Gratis',
                    'position': '2',
                    'precioPromo': '157900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '300 megas | 2 Meses Gratis',
                    'position': '3',
                    'precioPromo': '202900.00',
                    'categoriaPromo': 'Friba',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '50 megas',
                    'position': '00',
                    'precioPromo': '70900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '100 megas | 1 Meses Gratis',
                    'position': '01',
                    'precioPromo': '92900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '200 megas | 1 Meses Gratis',
                    'position': '02',
                    'precioPromo': '139900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                },
                {
                    'name': '300 megas | 1 Meses Gratis',
                    'position': '03',
                    'precioPromo': '184900.00',
                    'categoriaPromo': 'Fibra',
                    'eTailer': 'Targeters'
                }
            ],
        },
    },
    'event': 'promotionClick'
});

//Términos y Condiciones
dataLayer.push({
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'tyc',
    'eventLabel': 'Términos y Condiciones',
    'eTailer': 'Targeters'
}, {
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'tyc',
    'eventLabel': 'Cobertura Fibra Óptica',
    'eTailer': 'Targeters'
});

//Beneficios
dataLayer.push({
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'beneficios | fibra',
    'eventLabel': 'Matonea si parar',
    'eTailer': 'Targeters'
}, {
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'beneficios | fibra',
    'eventLabel': 'Modo Gamer',
    'eTailer': 'Targeters'
}, {
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'beneficios | fibra',
    'eventLabel': 'Movistar Música',
    'eTailer': 'Targeters'
}, {
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'beneficios | fibra',
    'eventLabel': 'App Smart Wifi',
    'eTailer': 'Targeters'
}, {
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'beneficios | fibra',
    'eventLabel': 'Más para tu hogar',
    'eTailer': 'Targeters'
}, {
    'event': 'trackEvent',
    'eventCategory': 'etailers',
    'eventAction': 'beneficios | fibra',
    'eventLabel': 'Acércate a lo que quieres',
    'eTailer': 'Targeters'
});