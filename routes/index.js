var express = require('express');
var router = express.Router();
const sendcontact = require('../libs/send-request');
const sendcontact2 = require('../libs/send-request2');
const sendcontactsocial = require('../libs/send-request-social');
const dbcore = require('mariadb');
const sendmail = require('nodemailer');
const { sprintf } = require('sprintf-js');
var request = require("request");
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Distribuidor Oficial Movistar', page: 'fibra', gracias: false, idInputF: "form_telefono_footer_hogar",
   dataCarrusel: [
     {id: 1, imgUrlDesktop: '/img/carousel/Banner_TRÍO2.png', imgUrlMobile: '/img/carousel/Banner_TRÍO.png', title: 'INTERNET FIBRA MOVISTAR', description_1: 'Activa este plan online', description_2: 'y paga menos'}, 
     {id: 2, imgUrlDesktop: '/img/carousel/bannerHogarFamily.jpg', imgUrlMobile: '', title: 'INTERNET FIBRA MOVISTAR', description_1: 'Activa este plan online', description_2: 'y paga menos'}, 
     {id: 3, imgUrlDesktop: '/img/carousel/carousel-2-1.png', imgUrlMobile: '', title: '', description_1: '', description_2: ''}
    ] });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
    console.log(celular);
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

//  router.get('/index', function (req, res, next) {
//   res.redirect('/');
//  })
//  router.post('/index', function (req, res, next) {
//    res.redirect('/')
//  });

router.get('/portabilidad', function (req, res, next) {
  res.render('portabilidad', { title: 'Distribuidor Oficial Movistar', page: 'portabilidad', gracias: false, idInputF: "form_telefono_footer",
  dataCarrusel: [
    // {id: 1, imgUrlDesktop: '/img/carousel/bannerHogarFamily.png', imgUrlMobile: '', title: '¡Es tiempo de compartir!', description_1: 'Conoce los nuevos planes', description_2: 'pospago Familia y Amigos', per: false}, 
    {id: 2, imgUrlDesktop: '/img/carousel/bannerHogarFamily.jpg', imgUrlMobile: '', title: '', description_1: '', description_2: '', per: true }, 
    {id: 3, imgUrlDesktop: '/img/carousel/carousel-3.png', imgUrlMobile: '', title: '', description_1: '', description_2: '', per: true}, 
    {id: 4, imgUrlDesktop: '/img/carousel/carousel-3.png', imgUrlMobile: '', title: '', description_1: '', description_2: '', per: false}, 
   ]
});
});

router.post('/llamar', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontact.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});


router.get('/gracias', function (req, res, next) {
  res.render('fibra/gracias', { title: 'Distribuidor Oficial Movistar', page: 'fibra', gracias: true, idInputF: "form_telefono_footer_hogar" });
});

router.post('/gracias', function (req, res, next) {
  res.render('fibra/gracias', { title: 'Distribuidor Oficial Movistar', page: 'fibra', gracias: true, idInputF: "form_telefono_footer_hogar" });
});

// router.get('/graciashogares', function (req, res, next) {
//   res.render('gracias', { title: 'Gracias' });
// });

// router.post('/graciashogares', function (req, res, next) {
//   res.render('gracias', { title: 'Gracias' });
// });

router.get('/hogar/gracias', function (req, res, next) {
  res.render('hogar/gracias', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: true, idInputF: "form_telefono_footer_hogar" });
});

router.post('/hogar/gracias', function (req, res, next) {
  res.render('hogar/gracias', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: true, idInputF: "form_telefono_footer_hogar" });
});
router.get('/fibra/gracias', function (req, res, next) {
  res.render('fibra/gracias', { title: 'Distribuidor Oficial Movistar', page: 'fibra', gracias: true, idInputF: "form_telefono_footer_hogar" });
});
router.post('/fibra/gracias', function (req, res, next) {
  res.render('fibra/gracias', { title: 'Distribuidor Oficial Movistar', page: 'fibra', gracias: true, idInputF: "form_telefono_footer_hogar" });
});
router.get('/portabilidad/gracias', function (req, res, next) {
  res.render('portabilidad/gracias', { title: 'Distribuidor Oficial Movistar', page: 'portabilidad', gracias: true, idInputF: "form_telefono_footer" });
});
router.post('/portabilidad/gracias', function (req, res, next) {
  res.render('portabilidad/gracias', { title: 'Distribuidor Oficial Movistar', page: 'portabilidad', gracias: true, idInputF: "form_telefono_footer" });
});
router.get('/pospago/gracias', function (req, res, next) {
  res.render('pospago/gracias', { title: 'Distribuidor Oficial Movistar', page: 'pospago', gracias: true, idInputF: "form_telefono_footer" });
});
router.post('/pospago/gracias', function (req, res, next) {
  res.render('pospago/gracias', { title: 'Distribuidor Oficial Movistar', page: 'pospago', gracias: true, idInputF: "form_telefono_footer" });

});

// router.get('/terminosycondicionesmovil', function (req, res, next) {
//   res.render('terminosycondicionesmovil', { title: 'terminosycondicionesmovil' });
// });

// router.post('/terminosycondicionesmovil', function (req, res, next) {
//   res.render('terminosycondicionesmovil', { title: 'terminosycondicionesmovil' });

// });

// router.get('/graciashogares', function (req, res, next) {
//   res.render('graciashogares', { title: 'Gracias' });
// });

// router.post('/graciashogares', function (req, res, next) {
//   res.render('graciashogares', { title: 'Gracias' });

// });

router.get('/pospago', function (req, res, next) {
  res.render('pospago', { title: 'Distribuidor Oficial Movistar', page: 'pospago', gracias: false, idInputF: "form_telefono_footer" });
});

router.post('/llamar', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontact.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});


// router.post('/guardar', function (req, res, next) {
//   var body = req.body;
//   console.log('entro al guardar', body);
//   res.status(200);
//   sendcontact2.request(body, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos guardar contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos guardar contigo' + body);

// });



// router.get('/fibra', function (req, res, next) {
//   res.render('fibra', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamardos', function (req, res) {
//   var celular = req.body.numero; 
//   var slug = req.body.slug;
//   res.status(200);
//   sendcontactsocial.request(celular,slug, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });


router.get('/hogar', function (req, res, next) {
  res.render('hogar', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

// router.get('/Fonseca', function (req, res, next) {
//   res.render('Fonseca', { title: 'Distribuidor Oficial Movistar' });
// });

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_cali', function (req, res, next) {
  res.render('hogar_cali', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_barranquilla', function (req, res, next) {
  res.render('hogar_barranquilla', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_cartagena', function (req, res, next) {
  res.render('hogar_cartagena', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_ibague', function (req, res, next) {
  res.render('hogar_ibague', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_cucuta', function (req, res, next) {
  res.render('hogar_cucuta', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_villavicencio', function (req, res, next) {
  res.render('hogar_villavicencio', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_santamarta', function (req, res, next) {
  res.render('hogar_santamarta', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar" });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

router.get('/hogar_bogota', function (req, res, next) {
  res.render('hogar_bogota', { title: 'Distribuidor Oficial Movistar', page: 'hogar', gracias: false, idInputF: "form_telefono_footer_hogar"  });
});

router.post('/llamardos', function (req, res) {
  var celular = req.body.numero;
  var slug = req.body.slug;
  res.status(200);
  sendcontactsocial.request(celular,slug, (e, r, b) => {
    if (e) {
      console.log(e);
      return res.send('Pronto nos comunicaremos contigo');
    }
    if (b) {
      console.log(b);
    }
  });

  res.send('Pronto nos comunicaremos contigo' + celular);

});

// router.post('/guardar', function (req, res, next) {
//   var body = req.body;
//   console.log('entro al guardar', body);
//   res.status(200);
//   sendcontact2.request(body, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos guardar contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos guardar contigo' + body);

// });





// router.get('/cartagena', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });


// router.get('/bucaramanga', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/cucuta', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/tunja', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/ibague', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/armenia', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/pereira', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/manizales', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/santamarta', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/pasto', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/villavicencio', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/popayan', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

// router.get('/neiva', function (req, res, next) {
//   res.render('index', { title: 'Distribuidor Oficial Movistar' });
// });

// router.post('/llamar', function (req, res) {
//   var celular = req.body.numero;
//   res.status(200);
//   sendcontact.request(celular, (e, r, b) => {
//     if (e) {
//       console.log(e);
//       return res.send('Pronto nos comunicaremos contigo');
//     }
//     if (b) {
//       console.log(b);
//     }
//   });

//   res.send('Pronto nos comunicaremos contigo' + celular);

// });

router.get('/error', (req, res, next) => {
  res.redirect('/');
});

module.exports = router;
