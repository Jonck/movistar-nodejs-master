var request = require('request');

module.exports.request = (field,slug, cb) => {
    let serie = {'authorization':'AAAA-SO',"numero": field, "id": (field.length == 10 ? 1 : 2 ), "extra": { "tsource":slug } };
    console.log(serie);
    request({
        method: 'POST',
        url: "https://so11.kerberusipbx.com:625/api/v0.1/callback",
        headers: {
            'authorization':'AAAA-SO',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(serie)
    }, (error, response, body) => {
        cb(error, response, body)
    });
};